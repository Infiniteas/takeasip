
package midterm;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jasmine
 */
public class StatClass {
    
    //statistic values for an array or list
    private int mode;
    private int min;
    private int max;
    private int average;
    private int nModes;
    private int freq;
    
    Vector array = new Vector();
    Vector modes = new Vector();
    
    DoublyLinkedList list = new DoublyLinkedList();
    DoublyLinkedList modesLL = new DoublyLinkedList();
    
    //Constructor
    StatClass(){
        mode = 0;
        min = 0;
        max = 0;
        average = 0;
        nModes = 0;
        freq = 0;
    }
    
    
    //setters
    public void setVector(Vector x){
        this.array = x;
    }
    
    public void setDoublyLinkedList(DoublyLinkedList l){
        this.list = l;
    }
    
    //getters
    public int getMode(){
        return mode;
    }
    
    public int getMin(){
        return min;
    }
    
    public int getMax(){
        return max;
    }
    
    public int getAverage(){
        return average;
    }
    
    public int getNModes(){
        return nModes;
    }
    
    public int getFreq(){
        return freq;
    }
    
    public Vector getModes(){
        return modes;
    }
    
    public DoublyLinkedList getModesLL(){
        return modesLL;
    }
    
    
    /**
     * Returns the mode using HashMaps for Vectors
     * 
     * @return 
     */
    public int modeMapVectors(){
        
        HashMap<Integer,Integer> frequencys = new HashMap<Integer,Integer>();
        
        int[] values = array.sortArray();

        for (int val : values) {
            Integer freq = frequencys.get(val);
            frequencys.put(val, (freq == null ? 1 : freq+1));
        }
        
        int mode = 0;
        int maxFreq = 0;
        
        //loops to retrieve the key
        for (Map.Entry<Integer,Integer> entry : frequencys.entrySet()) {
            int freq = entry.getValue();
            
            //if the frequency is more than one
            if(freq > 1){
                this.modes.add(entry.getKey()); //add to modes array
                nModes++; //increments with numbers of modes
            }
            
            //finds the mode with the highest frequency
            if (freq > maxFreq) {
                
                maxFreq = freq;
                this.freq = maxFreq;  //frequency of the max mode
                mode = entry.getKey();
            }
        }
        
        //sets the mode
        this.mode = mode;
        
        return mode;
    }
    
    /**
     * Returns the mode using HashMaps for Linked Lists
     * 
     * @return 
     */
    public int modeMapLinkedLists(){
        
        HashMap<Integer,Integer> frequencys = new HashMap<Integer,Integer>();
        
        Link current = list.getHeadLink();
        
        //Searches through the linked list
        while(current != null){
            
            Integer freq = frequencys.get(current.getItem());
            frequencys.put(current.getItem(), (freq == null ? 1 : freq+1));
            
            current = current.next;
        }
        
        int mode = 0;
        int maxFreq = 0;
        
        //loops to retrieve the key
        for (Map.Entry<Integer,Integer> entry : frequencys.entrySet()) {
            int freq = entry.getValue();
            
            //if the frequency is more than one
            if(freq > 1){
                this.modesLL.insertNewLink(entry.getKey()); //add to modes list
                nModes++; //increments with numbers of modes
            }
            
            //finds the mode with the highest frequency
            if (freq > maxFreq) {
                
                maxFreq = freq;
                this.freq = maxFreq; //frequency of the max mode
                mode = entry.getKey();
            }
        }
        
        //sets the mode
        this.mode = mode;
        
        return mode;
    }    
    
    
    /**
     * Returns the maximum value for Vectors
     * 
     * @return
     */
    public int max(){
        
        int[] sorted = array.sortArray();
        int maxValue = sorted[0];  
        
        for(int i = 1; i < sorted.length; i++){  
            if(sorted[i] > maxValue){  
                maxValue = sorted[i];  
            }  
        }  
        
        this.max = maxValue;
        return maxValue;
    }
    
    /**
     * Finds and sets the maximum value in the Linked List
     */
    public void maxLL(){
        
        Link current = list.getHeadLink();
        
        //sets first value to compare
        int maxValue = current.getItem();
        
        //searches through the linked list
        while(current.next != null){
            
            //compares which link has the biggest value
            if(current.next.getItem() > current.getItem()) {
                maxValue = current.next.getItem();  //sets new max
            }
            current = current.next;
        }
        
        this.max = maxValue;
    }
    
    /**
     * Returns the minimum value for Vectors
     * 
     * @return
     */
    public int min(){
        
        int[] sorted = array.sortArray();
        int minValue = sorted[0];  
        
        for(int i = 1; i < sorted.length; i++){
            if(sorted[i] != 0) {
                if(sorted[i] < minValue){  
                    minValue = sorted[i];  
                }
            }
        }  
        this.min = minValue;
        return minValue;        
    }
    
    /**
     * Finds and sets the minimum value in the Linked List
     */
    public void minLL(){
        
        Link current = list.getHeadLink();
        
        //set's the first value to compare
        int minValue = current.getItem();
        
        //searches through the linked list
        while(current.next != null){
            
            //compares which link has the smallest value
            if(current.next.getItem() < current.getItem()) {
                minValue = current.next.getItem(); //sets new min
            }
            
            current = current.next;
        } 
        this.min = minValue;
    }
    
    /**
     * Returns the average value for vectors
     * 
     * @return
     */
    public int average(){
        
        int avg = 0;
        int n = 0;
        int[] sorted = array.sortArray();
        
        for(int i = 0; i < sorted.length; i++){
            if(sorted[i] != 0){
                avg += sorted[i];
                n++;
            }
        } 
        this.average = (avg / n);
        return (avg / n);
    }
    
    /**
     * Finds and sets the average value in the Linked List
     */
    public void averageLL(){
        int avg = 0;
        int n = 0;
        
        Link current = list.getHeadLink();
        
        //searches through the linked list
        while(current != null){
            
            //adds all the values in the linked list
            avg += current.getItem();
            n++;                          //increments amount in list
            current = current.next;
        } 
        
        this.average = (avg / n);
    }
    
}
