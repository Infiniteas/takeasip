/*
 * File: Midterm
 * Programmer: Jasmine Anica
 * Class: CSC 18C
 * Date: 4/29/15
 */
package midterm;

import java.util.Random;


/**
 *
 * @author Jasmine
 */
public class Main {
    
    public static void main(String[] args) {
        problemEight();
       
    } 
    
    public static void problemOne(){
        
        Vector vect1 = new Vector();
        Vector top = new Vector();

        Random rand = new Random();
        int upperBound = 100;
        int lowerBound = 10;
        
        //number of values to add to the vector
        int numberOfValues = 40;
       
        //adds random values to the vector
        for (int i = 0; i < numberOfValues; i++) {
            vect1.add(rand.nextInt(upperBound - lowerBound) + lowerBound);
        }
              
        System.out.println("A vector filled with " + numberOfValues 
                + " random values between " + lowerBound + "-" + upperBound);
        
        //prints out the original vector
        vect1.print(10);
        System.out.println();
        
        //number of top values
        int p = 5;
        
        //calls the top function and returns a vector with the top values
        top = top(vect1, p);
        
        //prints out the top vector
        System.out.println("The top " + p + " values in the vector");
        top.print(10);
        System.out.println();
    }
    
    
    /**
     * This function takes a vector and returns a new vector with the top
     * p number of values in the original vector.
     * 
     * @param x
     * @param p
     * @return 
     */
    public static Vector top(Vector x, int p){
        Vector top = new Vector();
        int[] sorted = x.sortArray();
        
        //prints out the sorted vector
        System.out.println("Sorted vector: ");
        print(10, sorted);
        System.out.println();
        
        //copies the top p numbers in the array
        for (int i = 0; i < p; i++){
            top.add(sorted[(sorted.length - p) + i]);
        }
        
        return top;
    }
    
    public static void problemTwo(){
        
        //vect2 will contain no modes
        Vector vect2 = new Vector();       
        //vect3 will contain 1 mode
        Vector vect3 = new Vector(); 
        //vect4 will contain multimodes
        Vector vect4 = new Vector();
        
        //m determines the values inside the array
        int m = 30;
        
        Vector mode = new Vector();
        
        int num = 1;
        for(int i = 0; i < m; i++){
            vect2.add(num);
            num++;
        }
        
        System.out.println("A vector filled with " + m 
                + " values between " + 1 + "-" + m);
        //prints out the original array
        vect2.print(10);
        System.out.println();
        
        //checks for the mode and returns mode vector
        mode = mode(vect2);
        
        num = 1;
        for(int i = 0; i < m/2; i++){
            vect3.add(num);
            num++;
        }
        
        vect3.add(num);
        
        for(int i = 0; i < m/2 - 1; i++){
            vect3.add(num);
            num++;
        }
        
        System.out.println("A vector filled with " + m + 
                " values between " + 1 + "-" + m);
        //prints out the original array
        vect3.print(10);
        System.out.println();
        
        //calls mode function
        mode = mode(vect3);
        
        System.out.print("Mode: ");
        mode.print(10);
        System.out.println();
        
        //creates random numbers
        Random rand = new Random();
        int upperBound = m;
        int lowerBound = 1;
        
        //add random values to the array
        for (int i = 0; i < m; i++) {
            vect4.add(rand.nextInt(upperBound - lowerBound) + lowerBound);
        }
        
        System.out.println();
        System.out.println("A vector filled with " + m + 
                " random values between " + lowerBound + "-" + upperBound);
        //prints out the original array
        vect4.print(10);
        System.out.println();


        //creates a mode vector
        mode = mode(vect4);
        
        System.out.println("Multiple Modes: ");
        //prints out the mode vector
        mode.print(10);
        System.out.println();
                  
    }
    
    /**
     * This function will return a vector with the total modes
     * If there are no modes, the function will output "No Mode".
     * 
     * @param x
     * @return 
     */
    public static Vector mode(Vector x){
                
        int[] sorted = x.sortArray();
        Vector mode = new Vector();
       
        //prints out the sorted vector
        System.out.println("Sorted vector: ");
        print(10, sorted);
        System.out.println();
        
        for(int i = 0; i < sorted.length-1; i++) {
            if(sorted[i] == sorted[i+1] && sorted[i] != 0) {
                //check to make sure the mode vector does not duplicate values
                if(mode.checkNoMuliple(sorted[i])){ 
                    mode.add(sorted[i]);
                }
            }
        }
        
        if(mode.isEmpty()){
            System.out.println("No Modes");
            System.out.println();
        }
        
      return mode;
    }
    
    public static void problemThree(){
        Vector vect5 = new Vector();
        Vector modes = new Vector();
        
        int m = 40;
        
        //creates random numbers
        Random rand = new Random();
        int upperBound = m;
        int lowerBound = 1;
        
        //add random values to the array
        for (int i = 0; i < m; i++) {
            vect5.add(rand.nextInt(upperBound - lowerBound) + lowerBound);
        }
        
        System.out.println();
        System.out.println("A vector filled with " + m + 
                " random values between " + lowerBound + "-" + upperBound);
        //prints out the original vector
        vect5.print(10);
        System.out.println();
        
        int[] sorted = vect5.sortArray();
        
        //prints out the sorted vector
        System.out.println("Sorted vector: ");
        print(10, sorted);
        System.out.println();
        
        StatClass stats = stat(vect5);
        
        System.out.println("Mode: " + stats.getMode());
        System.out.println("Freq: " + stats.getFreq());
        System.out.println("Min: " + stats.getMin());
        System.out.println("Max: " + stats.getMax());
        System.out.println("Average: " + stats.getAverage());
        System.out.println("nModes: " + stats.getNModes());
        
        
        modes = stats.getModes();
        System.out.print("Modes: ");
        modes.print(10);
        
        System.out.println();
        System.out.println();
    }
    
    /**
     * This function takes a vector and stores the stats in StatClass
     * 
     * @param x
     * @return 
     */
    public static StatClass stat(Vector x){
        
        StatClass info = new StatClass();
        
        info.setVector(x);
        info.modeMapVectors();
        info.min();
        info.max();
        info.average();

        return info;
    }
    
    public static void problemFour(){
        
        DoublyLinkedList listOne = new DoublyLinkedList();
        DoublyLinkedList top = new DoublyLinkedList();
        
        Random rand = new Random();
        int upperBound = 100;
        int lowerBound = 10;
        
        //number of values to add to the vector
        int numberOfValues = 40;
       
        //adds random values to the vector
        for (int i = 0; i < numberOfValues; i++) {
            listOne.insertNewLink(rand.nextInt(upperBound - lowerBound) + lowerBound);
        }
        
        System.out.println("Sorted linked list with " + numberOfValues 
                + " random values between " + lowerBound + "-" + upperBound);
        
        //prints the linkedlist (sorted automatically)
        listOne.printSortedLinkedList();
        System.out.println();
        
        //number of top values from listOne
        int p = 5;
        top = top(listOne, p);
        
        System.out.println("Top " + p + " values in the LinkedList: ");
        
        //prints out top linked list
        top.printSortedLinkedList();
        System.out.println();
        
    }
    
    
    /**
     * 
     * This function will return a doubly linked list with the top p
     * values in the original doubly linked list passed in.
     * 
     * @param x
     * @param p
     * @return 
     */
    public static DoublyLinkedList top(DoublyLinkedList x, int p){
        DoublyLinkedList top = new DoublyLinkedList();
        
        Link theLink = x.getTailLink();
        int count = 0;
        while(count < p){
            top.insertNewLink(theLink.getItem());
            theLink = theLink.previous;
            count++;
        }
        
        return top;
    }
    
    public static void problemFive(){
        
        //listOne will have no mode
        DoublyLinkedList listOne = new DoublyLinkedList();
        //listTwo will have 1 mode
        DoublyLinkedList listTwo = new DoublyLinkedList();
        //listThree will have multimodes
        DoublyLinkedList listThree = new DoublyLinkedList();
        
        DoublyLinkedList mode = new DoublyLinkedList();
        
        // number of values to add to the linked list
        int m = 60;
        
        int num = 1;
        
        //fills listOne with %m values
        for (int i = 0; i < m; i++){
            listOne.insertNewLink(num);
            num++;
        }
        
        System.out.println("Linked list filled with %" + m + " values");
        listOne.printSortedLinkedList();
        System.out.println();
        
        //finds the mode in listOne
        mode = mode(listOne);
        //prints the mode list
        mode.printSortedLinkedList();
        System.out.println();
        
        num = 1;
        //fills listTwo with %m values
        for (int i = 0; i < m-1; i++){
            listTwo.insertNewLink(num);
            if(num == 45){
                listTwo.insertNewLink(num);
            }
            num++;
        }
        
        System.out.println("Link List filled with %" + m + " values" );
        
        listTwo.printSortedLinkedList();
        System.out.println();
        
        //finds the modes in listTwo and returns a mode linked list
        mode = mode(listTwo);
        
        //prints the mode linked list
        System.out.print("Mode:  ");
        mode.printSortedLinkedList();
        System.out.println();
        
        Random rand = new Random();
        int upperBound = m;
        int lowerBound = 1;
        
        //fills listOne with random %m values
        for (int i = 0; i < m; i++){
            listThree.insertNewLink(rand.nextInt(upperBound - lowerBound) + 
                    lowerBound);
        }
        System.out.println();
        System.out.println("Linked List filled with random %" + m + " values");
        listThree.printSortedLinkedList();
        System.out.println();
        
        mode = mode(listThree);
        System.out.println("Mode:  ");
        mode.printSortedLinkedList();
        System.out.println();
        
        
    }
    
    /**
     * This function will return a linked list with the total modes
     * from the linked list passed in
     * 
     * @param x
     * @return
     */
    public static DoublyLinkedList mode(DoublyLinkedList x){
        
        //Starts at the beginning of the doubly linked list
        Link theLink = x.getHeadLink();
        
        //This linked list will hold the modes 
        DoublyLinkedList mode = new DoublyLinkedList();
        
        //Searches through the linked list x
        while(theLink.next != null){
            
            //finds an equivalent link
            if (theLink.next.compareTo(theLink) == 0){
                
                //if the mode is empty will add the item to mode list
                if(mode.isLinkedListEmpty()){
                    
                    //adds the value to the mode list
                    mode.insertNewLink(theLink.getItem());
                    
                    //will check if value already exists in mode list
                } else if(mode.checkNoMulti(theLink.getItem())){
                    
                    //adds mode to list if value does not already exist
                    mode.insertNewLink(theLink.getItem());
                }
            }
            
            theLink = theLink.next;
        }
        
        //Checks if mode list is empty
        if (mode.isLinkedListEmpty()){
            System.out.println("No Mode");
        }
        
        return mode;
    }
    
    
    public static void problemSix(){
        DoublyLinkedList listFour = new DoublyLinkedList();
        DoublyLinkedList mode = new DoublyLinkedList();
        
        int m = 80;
        
        //creates random numbers
        Random rand = new Random();
        int upperBound = m;
        int lowerBound = 1;
        
        //add random values to the array
        for (int i = 0; i < m; i++) {
            listFour.insertNewLink(rand.nextInt(upperBound - lowerBound) + lowerBound);
        }
        
        System.out.println();
        System.out.println("A sorted Linked List filled with " + m + 
                " random values between " + lowerBound + "-" + upperBound);
        //prints out the original array
        listFour.printSortedLinkedList();
        System.out.println();
        
        StatClass stats = stat(listFour);
        
        System.out.println("Mode: " + stats.getMode());
        System.out.println("Freq: " + stats.getFreq());
        System.out.println("Min: " + stats.getMin());
        System.out.println("Max: " + stats.getMax());
        System.out.println("Average: " + stats.getAverage());
        System.out.println("nModes: " + stats.getNModes());
        
        
        mode = stats.getModesLL();
        System.out.println("Modes: ");
        mode.printSortedLinkedList();
        
        System.out.println();
        System.out.println();
    }
    
    public static StatClass stat(DoublyLinkedList l){
        
        StatClass info = new StatClass();
        
        info.setDoublyLinkedList(l);
        info.modeMapLinkedLists();
        info.minLL();
        info.maxLL();
        info.averageLL();

        return info;
    }
    
    
    public static void problemSeven(){
        
        SelfOrganizingList listSix = new SelfOrganizingList();
        
        //amount of values in the list
        int m = 20;
        
        Random rand = new Random();
        int upperBound = m;
        int lowerBound = 1;
        
        //fills listOne with random %m values
        for (int i = 0; i < m; i++){
            listSix.insertNewLink(rand.nextInt(upperBound - lowerBound) + 
                    lowerBound);
        }
        System.out.println();
        System.out.println("Linked List filled with random %" + m + " values");
        listSix.printSortedLinkedList();
        System.out.println();
        
        //values that we will search for in the list
        int valueOne = 3;
        int valueTwo = 12;
        int valueThree = 17;
        
        System.out.println("Search through the list for: " + valueOne + "  " +
                valueTwo + "  " + valueThree + " repectively");
        System.out.println();
        
        if(listSix.findLink(valueOne)){
            System.out.println("Link with " + valueOne + " found!");
            
        }
        
        if(listSix.findLink(valueTwo)){
            System.out.println("Link with " + valueTwo + " found!");
        }
        
        if(listSix.findLink(valueThree)){
            System.out.println("Link with " + valueThree + " found!");
        }
        
        
        System.out.println();
        System.out.println("Linked List after searches");
        listSix.printSortedLinkedList();
        System.out.println();
    }
    
    public static void problemEight(){
        
        //base
        int x = 2;
        
        //exponent
        int y = 10;
        
        //calles recursive function
        float answer = powR(x, y);
        
        //prints out the answer
        System.out.println("power:  " + answer);
        
       
    }
    
    /**
     *
     * This is a recursive function that passes in a base x and an exponent y
     *  to return x^y
     * 
     * @param x
     * @param y
     * @return
     */
    public static float powR(float x, int y){

        //base case
        if (y == 1){
            return x;
        } else {
            
            if (y % 2 == 0){                              // if y is even
                float a = powR(x, y/2);
                return a * a;
            } else {                                     // if y is odd
                float a = powR(x, y-1);
                return a * x;
            }
        }
       
    }
    
    /**
     * This function prints out an int[] 
     * 
     * @param perLine
     * @param arr 
     */
    public static void print(int perLine, int[] arr){
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] != 0) {
                if(arr[i] < 10) {
                    System.out.print(arr[i] + "   ");

                } else {
                    System.out.print(arr[i] + "  ");
                }
                if(i % perLine == (perLine - 1)) {
                    System.out.println();
                }
            }
        }
    }
    
    
}
