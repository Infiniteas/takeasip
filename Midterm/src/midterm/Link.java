
package midterm;

/**
 *
 * @author Jasmine
 */
public class Link implements Comparable<Link> {
    
    //reference to the next link
    Link next;
    //reference to the previous link
    Link previous;

    //number in this link
    private Integer item;
    
        
    //adds item to this link
    public void addItem(int item) {
        this.item = item;
    }
    
    //displays item in this link
    public void printLink() {
        if(item < 10){
            System.out.print(item + "   ");
        } else {
            System.out.print(item + "  ");
        }
    }
    
    public Integer getItem(){
        return item;
    }
    
    @Override
    public int compareTo(Link that) {
       //will return 0 if item are equal
       //will return 1 if item is greater
       //will return -1 if item is less
       return getItem().compareTo(that.getItem());
    }
    
        
}