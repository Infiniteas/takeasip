
package midterm;

/**
 *
 * @author Jasmine
 */
public class SelfOrganizingList extends DoublyLinkedList {
    
    /**
     *
     * This function returns true if the link with the passed in item exists and
     * moves the link to the front of the list
     * 
     * @param item
     * @return
     */
    public boolean findLink(Integer item) {
        
        //holds the link that we want found
        Link find = new Link();
        find.addItem(item);
        
       //if list is empty, will return a null link
        if (isLinkedListEmpty()) { 
            System.out.println("Linked List is empty!");
            return false;
        }
        
        Link current = getHeadLink();
        
        //searches through the list until it finds a match
        while (current != null && current.compareTo(find) != 0) {
            current = current.next;
        }
        
        if(current == null){
            System.out.println("Link with " + item + " NOT found!");
            return false;
        }
        
        //stores the first link the list
        Link first = getHeadLink();
        
        //deletes the current if it is not already the headLink
        if (current.compareTo(first) != 0){
           
            //deletes the link in the list
            current.next.previous = current.previous;
            current.previous.next = current.next;
            
            //moves the found link to the front
            this.headLink.previous = current;
            current.next = this.headLink;
            this.headLink = current;
        } 
        
        return true;
    }
}
